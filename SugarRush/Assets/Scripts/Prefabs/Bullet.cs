using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class Bullet : MonoBehaviour
{
    float bulletSpeed = 2;
    Vector3 bulletDirection;
    Rigidbody rb;

    public void SetBulletParameters(Vector3 direction, float speed)
    {
        bulletDirection = direction;
        bulletSpeed = speed;
    }
    

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        rb.MovePosition(rb.position + bulletDirection * bulletSpeed * Time.deltaTime);
    }
}
