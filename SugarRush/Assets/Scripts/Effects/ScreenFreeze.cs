using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenFreeze : MonoBehaviour
{
    public Camera mainCamera;

    public float defaultFreezeShakeDuration = 0.1f;
    public float defaultshakeMagnitude = 0.001f;

    bool isFrozen;
    bool isShaking;
    bool isSlowMo;

    float shakeScale;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Freeze()
    {
        Freeze(defaultFreezeShakeDuration);
    }

    public void Freeze(float duration)
    {
        if (!isFrozen)
        {
            StartCoroutine(DoFreeze(duration));
        }
    }

    public void Shake() 
    {
        Shake(defaultFreezeShakeDuration, defaultshakeMagnitude);
    }

    public void Shake(float duration, float shakeScale)
    {
        if (!isShaking)
        {
            StartCoroutine(DoShake(duration, shakeScale));
        }
    }

    public void FreezeShake()
    {
        FreezeShake(defaultFreezeShakeDuration, defaultshakeMagnitude);
    }

    public void FreezeShake(float duration, float shakeScale)
    {
        if(!isShaking && !isFrozen)
        {
            StartCoroutine(DoFreeze(duration * 1.5f));
            StartCoroutine(DoShake(duration, shakeScale));
        }
    }

    public void SlowMoShake(float duration, float timeScale, float shakeScale)
    {
        if(!isSlowMo && !isShaking)
        {
            StartCoroutine(DoShake(duration, shakeScale));
            StartCoroutine(DoSlowMo(duration, timeScale));
        }
    }

    IEnumerator DoFreeze(float time)
    {
        isFrozen = true;
        float originalTimeScale = Time.timeScale;

        Time.timeScale = 0;

        yield return new WaitForSecondsRealtime(time);

        Time.timeScale = originalTimeScale;
        isFrozen = false;
    }
    IEnumerator DoSlowMo(float time, float newTimeScale)
    {
        isSlowMo = true;
        float originalTimeScale = Time.timeScale;

        Time.timeScale = newTimeScale;

        yield return new WaitForSecondsRealtime(time);

        Time.timeScale = originalTimeScale;
        isSlowMo = false;
    }

    IEnumerator DoShake(float duration, float shakeScale)
    {
        isShaking = true;
        Vector3 originalPos = transform.position;

        float timer = 0;

        while (timer < duration)
        {
            float x = Random.Range(-1f, 1f) * shakeScale;
            float z = Random.Range(-1f, 1f) * shakeScale;

            mainCamera.transform.position += new Vector3(x, 0, z);

            timer += Time.deltaTime;

            yield return 0;
        }
        isShaking = false;
    }
}
