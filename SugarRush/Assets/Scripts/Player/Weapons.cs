using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Weapons : MonoBehaviour
{
    public GameObject bullet;

    [SerializeField] float bulletSpeed = 1;
    [SerializeField] float pistolBulletCountMax = 6;
    [SerializeField] float pistolReloadTime = 1.5f;
    [SerializeField] float pistolBulletCooldown = 0.5f;

    public float currentPistolBulletCount = 0;

    private bool cooldownReady = true;

    private bool isReloading = false;

    private Coroutine reloadRoutine;
    private Coroutine cooldownRoutine;

    void Start()
    {
       
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
            Fire();
    }

    public void Reload()
    {
        if (isReloading == false)
        {
            reloadRoutine = StartCoroutine(Cooldown(pistolReloadTime));
        }
    }
    public void Fire()
    {
        //float bulletDispersion = Random.Range(((currentDispersion / 2) * -1), (currentDispersion / 2));

        if (cooldownReady && !isReloading && currentPistolBulletCount > 0)
        {
            cooldownReady = false;

            currentPistolBulletCount -= 1;

            GameObject tempBullet = Instantiate(bullet, this.transform.position + transform.forward, Quaternion.identity);
            tempBullet.GetComponent<Bullet>().SetBulletParameters(transform.forward, bulletSpeed);

            cooldownRoutine = StartCoroutine(Cooldown(pistolBulletCooldown));
        }
    }

    IEnumerator Cooldown(float cooldown)
    {
            yield return new WaitForSeconds(cooldown);
            cooldownReady = true;
    }
}
