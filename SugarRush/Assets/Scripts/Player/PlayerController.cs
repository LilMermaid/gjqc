using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public Camera playerCamera;
    
    public float playerSpeed = 1;

    public float cameraDamping = 12.0f;
    public float cameraDistanceMax = 0.15f;
    public float cameraDistanceMaxAim = 0.3f;

    private float startSpeed = 0;
    private float startCamDistance = 0;
    private Vector3 movement;
    private Vector3 target;


    private bool isMoving;
    private bool isDead = false;

    private float boostSpeed = 1;
    private void Awake()
    {
        Cursor.lockState = CursorLockMode.Confined;
    }

    private void Start()
    {
        startSpeed = playerSpeed;
        startCamDistance = cameraDistanceMax;
    }
    private void Update()
    {

        if (movement.x == 0 && movement.z == 0)
        {
            isMoving = false;
            //animator.SetBool("isWalking", false);
        }
        else
        {
            isMoving = true;
            //animator.SetBool("isWalking", true);
        }

        if (!isDead)
        {
            Vector3 mouseDirection = new Vector3(0, 0, 0);

            Ray ray = playerCamera.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out RaycastHit hitInfo, maxDistance: 300f))
            {
                target = hitInfo.point;
                target.y = transform.position.y;
                mouseDirection = target - transform.position;

                //Rotation du personnage selon la vitesse de rotation maximale
                transform.rotation = Quaternion.LookRotation(mouseDirection);
            }

            movement.x = Input.GetAxisRaw("Horizontal");
            movement.z = Input.GetAxisRaw("Vertical");

            Rigidbody rb = this.GetComponent<Rigidbody>();


            rb.MovePosition(rb.position + Vector3.Normalize(movement * Time.deltaTime) * playerSpeed);



            //Mouvement cam�ra
            Vector3 camHeightPlayerPosition = new Vector3(this.transform.position.x, playerCamera.transform.position.y, this.transform.position.z);
            target.y = playerCamera.transform.position.y;
            Ray checkForWallRay = new Ray(camHeightPlayerPosition, Vector3.Normalize(target - camHeightPlayerPosition));

            Vector3 center;
            RaycastHit cameraWallHit;
            //new camera position
            if (Physics.Raycast(checkForWallRay, out cameraWallHit, maxDistance: Vector3.Distance(camHeightPlayerPosition, target) - 1))
            {
                //if there's wall
                center = camHeightPlayerPosition + (cameraDistanceMax * (cameraWallHit.point - camHeightPlayerPosition));
                center.y = playerCamera.transform.position.y;
            }
            else
            {
                //no wall
                center = camHeightPlayerPosition + (cameraDistanceMax * (target - camHeightPlayerPosition));
                center.y = playerCamera.transform.position.y;
            }

            //lerp between the two new position
            playerCamera.transform.position = Vector3.Lerp(playerCamera.transform.position, center, Time.deltaTime * cameraDamping);
        }
    }
}